import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminLTERoutingModule } from './admin-lte-routing.module';
import { FooterComponent } from './footer/footer.component';
import { SiderbarComponent } from './siderbar/siderbar.component';
import { ContainerComponent } from './container/container.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  imports: [
    CommonModule,
    AdminLTERoutingModule
  ],
  declarations: [ FooterComponent, SiderbarComponent, ContainerComponent, HeaderComponent],
  exports: [ FooterComponent, SiderbarComponent, ContainerComponent, HeaderComponent],
})
export class AdminLTEModule { }
