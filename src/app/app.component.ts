import { Component } from '@angular/core';
import { AdminLTEModule } from './admin-lte/admin-lte.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 
  /*
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |----------------------------------------------------------|
  | TEMA COLOR    1| skin-blue                               |
  |               2| skin-black                              |
  |               3| skin-purple                             |
  |               4| skin-yellow                             |
  |               5| skin-red                                |
  |               6| skin-green                              |
  |               7| skin-blue-light                         |
  |               8| skin-black-light                        |
  |               9| skin-purple-light                       |
  |              10| skin-yellow-light                       |
  |              11| skin-red-light                          |
  |              12| skin-green-light                        |
  |                                                          |
  |----------------------------------------------------------|
  |TEMA LAYOUT    1| fixed                                   |
  |               2| layout-boxed                            |                          
  |               3| sidebar-collapse                        |
  |               4| sidebar-mini                            |
  |----------------------------------------------------------|
 */

  public tema = 
  {
    color: 'skin-yellow-light',
    layout: 'sidebar-mini'
  }

  getTema()
  {
    return this.tema.color +' '+ this.tema.layout;
  }

}
