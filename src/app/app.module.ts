import {NgModule, ApplicationRef} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
//simport {DataTablesModule} from 'angular-datatables';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import { routing } from './app.routing';

//plantilla
import { AdminLTEModule } from './admin-lte/admin-lte.module';


//components
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    routing,
    AdminLTEModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
