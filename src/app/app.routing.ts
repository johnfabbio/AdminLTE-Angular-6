import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from "@angular/core";
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

export const routes: Routes = [
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: '**',
        redirectTo: 'home'       
    }

//
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {useHash: true});
